set -euo pipefail
if ${ONSTART:-false}; then
  source /usr/local/lib/wp-bootstraper.sh
  ln -nsf $PWD $WP_ROOT
  cp composer.json composer.local.json >& /dev/null
  #use a different prevent making unwanted changes to composer.json file
  export COMPOSER=composer.local.json
  composer update
  wp_install_admin
fi


