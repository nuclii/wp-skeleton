<?php
/**
 * Plugin Name:  System Plugin
 * Plugin URI:
 * Description:
 * Version:
 * Author:       Shoppress
 * Author URI:   https://myshoppress.com/
 * License:      MIT License
 */

use Roots\Bedrock\Autoloader;

if (is_blog_installed() && class_exists(Autoloader::class)) {
    new Autoloader();
}

if (defined('WP_ENV') && WP_ENV !== 'production' && !is_admin()) {
    add_action('pre_option_blog_public', '__return_zero');
}

if (!defined('WP_DEFAULT_THEME')) {
    register_theme_directory(ABSPATH . 'wp-content/themes');
}

?>
