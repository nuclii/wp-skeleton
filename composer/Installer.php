<?php

namespace MyShoppress\Composer;


use Composer\Composer;
use Composer\Downloader\PathDownloader;
use Composer\Installer\PackageEvent;
use Composer\Package\Package;
use Composer\Package\PackageInterface;
use Composer\Package\RootPackageInterface;
use UnexpectedValueException;

class Installer
{
    const PACKAGE_TYPE = 'wordpress-bundle';

    static public function prePackageUninstall(PackageEvent $event)
    {
        $package = $event->getOperation()->getPackage();
        self::performAction('uninstall', $package, $event->getComposer());
    }

    static public function postPackageInstall(PackageEvent $event)
    {
        $package = $event->getOperation()->getPackage();
        self::performAction('install', $package, $event->getComposer());
    }

    static private function performAction(string $action, PackageInterface $package, Composer $composer)
    {
        if (!isset($package->getExtra()[self::PACKAGE_TYPE]) ||
            !is_array($package->getExtra()[self::PACKAGE_TYPE]))
            return;
        $installationManager = $composer->getInstallationManager();
        $installer = $installationManager->getInstaller($package->getType());
        $sourcePath = $installer->getInstallPath($package);
        $subpackages = $package->getExtra()[self::PACKAGE_TYPE] ?? [];
        $pathDownloader = $composer->getDownloadManager()->getDownloader('path');
        foreach ($subpackages as $type => $paths) {
            if (!in_array($type, ['plugin', 'muplugin', 'theme', 'dropin'])) {
                continue;
            }
            $type = 'wordpress-' . $type;
            $typeInstaller = $installationManager->getInstaller($type);
            foreach ($paths as $path) {
                $subpackageName = basename($path);
                /** @var PathDownloader $pathDownloader */
                $dynamicPackage = new Package(
                    $subpackageName,
                    $package->getVersion(),
                    $package->getPrettyVersion()
                );
                $dynamicPackage->setType($type);
                $dynamicPackage->setInstallationSource('dist');
                $dynamicPackage->setDistType('path');
                $dest = $typeInstaller->getInstallPath($dynamicPackage);
                if ($action === 'install') {
                    $distUrl = $sourcePath . '/' . $path;
                    $dynamicPackage->setDistUrl($distUrl);
                    $pathDownloader->install($dynamicPackage, $dest);
                } else {
                    $pathDownloader->remove($dynamicPackage, $dest);
                }
            }
        }
    }
}
