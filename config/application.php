<?php
/**
 * Your base production configuration goes in this file. Environment-specific
 * overrides go in their respective config/environments/{{WP_ENV}}.php file.
 *
 * A good default policy is to deviate from the production config as little as
 * possible. Try to define as much of your configuration in this file as you
 * can.
 */

use Roots\WPConfig\Config;
use function Env\env;

/**
 * Directory containing all of the site's files
 *
 * @var string
 */
$root_dir = dirname(__DIR__);

/**
 * Document Root
 *
 * @var string
 */
$webroot_dir = $root_dir . '/web';

/**
 * Use Dotenv to set required environment variables and load .env file in root
 */
if ( file_exists($root_dir . '/.env') ) {
    $dotenv = Dotenv\Dotenv::createUnsafeImmutable($root_dir);
    $dotenv->load();
}

$checkRequired = function(...$envs) {
    foreach($envs as $env)  {
        if ( getenv($env) === null ) {
            throw new \RuntimeException(sprintf("'%s' is required", $env));
        }
    }
};

$checkRequired('HTTP_HOST');

if (!env('DB_URL')) {
    $checkRequired('DB_NAME','DB_USER','DB_PASSWORD');
}

/**
 * Set up our global environment constant and load its config first
 * Default: production
 */
define('WP_ENV', env('WP_ENV') ?: 'production');
define ( 'FS_METHOD', 'direct');
/**
 * URLs
 */
$requestScheme = $_SERVER['REQUEST_SCHEME'] ?? 'http';
$requestHost = $_SERVER['SERVER_NAME'] = $_SERVER['HTTP_HOST'] ?? env('HTTP_HOST');
$homeUrl = sprintf('%s://%s', $requestScheme, $requestHost);
Config::define('WP_HOME', $homeUrl);
Config::define('WP_SITEURL', $homeUrl);

/**
 * Custom Content Directory
 */
Config::define('CONTENT_DIR', '/app');
Config::define('WP_CONTENT_DIR', $webroot_dir . Config::get('CONTENT_DIR'));
Config::define('WP_CONTENT_URL', Config::get('WP_HOME') . Config::get('CONTENT_DIR'));

/**
 * DB settings
 */
Config::define('DB_NAME', env('DB_NAME'));
Config::define('DB_USER', env('DB_USER'));
Config::define('DB_PASSWORD', env('DB_PASSWORD'));
Config::define('DB_HOST', env('DB_HOST') ?: 'localhost');
Config::define('DB_CHARSET', 'utf8mb4');
Config::define('DB_COLLATE', 'utf8mb4_0900_ai_ci');
$table_prefix = env('DB_PREFIX') ?: 'wp_';


if (env('DB_URL')) {
    $dsn = (object) parse_url(env('DB_URL'));
    Config::define('DB_NAME', substr($dsn->path, 1));
    Config::define('DB_USER', $dsn->user);
    Config::define('DB_PASSWORD', isset($dsn->pass) ? $dsn->pass : null);
    Config::define('DB_HOST', isset($dsn->port) ? "{$dsn->host}:{$dsn->port}" : $dsn->host);
}

//set hyberdb config file
Config::define('DB_CONFIG_FILE', __DIR__.'/db-config.php');

/**
 * Authentication Unique Keys and Salts
 */
Config::define('AUTH_KEY', env('AUTH_KEY'));
Config::define('SECURE_AUTH_KEY', env('SECURE_AUTH_KEY'));
Config::define('LOGGED_IN_KEY', env('LOGGED_IN_KEY'));
Config::define('NONCE_KEY', env('NONCE_KEY'));
Config::define('AUTH_SALT', env('AUTH_SALT'));
Config::define('SECURE_AUTH_SALT', env('SECURE_AUTH_SALT'));
Config::define('LOGGED_IN_SALT', env('LOGGED_IN_SALT'));
Config::define('NONCE_SALT', env('NONCE_SALT'));

/**
 * Custom Settings
 */
Config::define('AUTOMATIC_UPDATER_DISABLED', true);
Config::define('WP_AUTO_UPDATE_CORE', true);

Config::define('ALLOW_UNFILTERED_UPLOADS', true);
Config::define('DISABLE_WP_CRON', true);
// Disable the plugin and theme file editor in the admin
Config::define('DISALLOW_FILE_EDIT', true);
// Disable plugin and theme updates and installation from the admin
Config::define('DISALLOW_FILE_MODS', true);

if ( env('ALLOW_INSTALL_PLUGINS') ) {
    Config::define('DISALLOW_FILE_EDIT', false);
    Config::define('DISALLOW_FILE_MODS', false);
}

// Limit the number of post revisions that Wordpress stores (true (default WP): store every revision)
Config::define('WP_POST_REVISIONS', env('WP_POST_REVISIONS') ?: true);

/**
 * Debugging Settings
 */
Config::define('WP_DEBUG_DISPLAY', false);
Config::define('WP_DEBUG_LOG', env('WP_DEBUG_LOG') ?? false);
Config::define('SCRIPT_DEBUG', false);
Config::define('WP_DEBUG', false);
ini_set('display_errors', '0');


/**
 * APCu Caching
 */
Config::define('WP_APCU_LOCAL_CACHE', true);
Config::define('CACHE_DIR', Config::get('WP_CONTENT_DIR').'/cache');

if( env('WP_CACHE_KEY_SALT')){
    Config::define('WP_CACHE_KEY_SALT', env('WP_CACHE_KEY_SALT'));
}

if( env('WP_CACHE')){
    Config::define('WP_CACHE', env('WP_CACHE'));
}

if( env('WP_CACHE_URL') ) {
    $cacheUrl = env('WP_CACHE_URL');
    $dsn = (object) parse_url($cacheUrl);
    if ( strpos($dsn->scheme, 'redis') !== false && isset($dsn->host) ) {
        $settings = [];
        parse_str($dsn->query ?? '', $settings);
        if ( strpos($dsn->scheme, 'redis+tls') === 0 ) {
            $settings['scheme'] = str_replace('redis+','', $dsn->scheme);
            $settings['host'] = $settings['scheme'].'://'.$dsn->host;
        }
        else {
            $settings['host'] = $dsn->host;
        }
        $settings['port'] = $dsn->port ?? 6379;
        if ( isset($dsn->path) ) {
            $settings['database'] = substr($dsn->path, 1);
        }
        if ( isset($dsn->pass) && $dsn->user ) {
            $settings['password'] = [$dsn->user, $dsn->pass];
        }
        else if ( isset($dsn->pass) ) {
            $settings['password'] = $dsn->pass;
        }
        foreach($settings as $key => $value) {
            Config::define('WP_REDIS_' . strtoupper($key), $value);
        }

        Config::define('WP_REDIS_IGNORED_GROUPS', ['coupons']);
    }
}

/**
 * Allow WordPress to detect HTTPS when used behind a reverse proxy or a load balancer
 * See https://codex.wordpress.org/Function_Reference/is_ssl#Notes
 */
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
    $_SERVER['HTTPS'] = 'on';
}

$env_config = __DIR__ . '/environments/' . WP_ENV . '.php';

if (file_exists($env_config)) {
    require_once $env_config;
}

/**
 * Bootstrap WordPress
 */
if (!defined('ABSPATH')) {
    define('ABSPATH', $webroot_dir . '/wp/');
}

//load all *.config.php files
foreach (glob(__DIR__ . '/*.config.php') as $filename) {
    require_once $filename;
}

Config::apply();

